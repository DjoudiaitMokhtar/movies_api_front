## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
### General Info
***
The movies_api_front project is the front part of the movies_api API, the project is developed with felgo.
## Technologies
***
A list of technologies used within the project:
* [Felgo](): Version 3.0
## Note
***

I have not yet created the registration page to connect to the API from the frontend using these identifiers:
* [User Name](): test2@mail.com
* [Password](): test2test2
