import Felgo 3.0
import QtQuick 2.8
import QtGraphicalEffects 1.0
import QtQuick.Window 2.15

Page {
  id: homePageItem
  title: 'Catalogue'


  LinearGradient {
    anchors.fill: parent
    start: Qt.point(0, 0)
    end: Qt.point(homePageItem.width * 0.2, homePageItem.width * 0.7)
    gradient: Gradient {
      GradientStop { position: 0.0; color: "#404040" }
      GradientStop { position: 0.7; color: "#121212" }
    }
  }


  GridView {
    id: movieslist
    anchors.horizontalCenter: parent.horizontalCenter
    width: parent.width
    height: parent.height
//    anchors.top: slideshow.bottom
    anchors.topMargin: dp(10)
    cellWidth: widths()
    cellHeight: cellWidth


    model: JsonListModel{
        id : listModel
        source: dataModel.movies
        fields: ["movie_id", "title", "poster"]

    }


    delegate: Item {
               width: movieslist.cellWidth +10
               height: movieslist.cellHeight + 30
               scale: 0.8
               Rectangle {
                   anchors.fill: parent
                   radius: dp(12)
                   border.color: "black"
                   border.width: dp(3)

       Image { source: poster
        anchors.fill: parent
        scale: 1
        y: 20;
        anchors.horizontalCenter: parent.horizontalCenter

    //hover image
        id: im1

            states: [ "mouseIn", "mouseOut" ]
            state: "mouseOut"

            transitions: [
                Transition {
                    from: "*"
                    to: "mouseIn"
                    NumberAnimation {
                        target: im1
                        properties: "scale"
                        from: 0.95
                        to: 1
                        duration: 400
                        easing.type: Easing.OutBounce
                    }
                }
            ]

            MouseArea{
                id: im1MouseArea
                hoverEnabled: true
                anchors.fill: parent

                onContainsMouseChanged: {
                    im1.state = containsMouse ? "mouseIn" : "mouseOut"
                }

               onClicked: {
                   console.debug(model.title)
                    page.navigationStack.popAllExceptFirstAndPush(detailPageComponent, {movie_id: model.movie_id })



               }
            }
         }
       }
    }
}


// component for creating detail pages
  Component {
      id: detailPageComponent


      DetailPage { }
}

// function to control the screen resolution
function  widths(){

      if (Window.width <400)
          {
        return page.width / 2
      }
      else if  (Window.width >= 400 && Window.width < 600)
      {
         return page.width / 3
      }
      else if  (Window.width >= 600 && Window.width < 800)
      {
         return page.width / 4
      }
      else if  (Window.width >= 800 && Window.width < 1200)
      {
         return page.width / 5
      }
      else if  (Window.width > 1200)
      {
         return page.width / 10
      }
  }
}



