import QtQuick 2.0
import Felgo 3.0
import QtGraphicalEffects 1.0
import QtQuick.Window 2.15



Page {

    id: searchPage
    title: qsTr("Add")

    LinearGradient {
      anchors.fill: parent
      start: Qt.point(0, 0)
      end: Qt.point(searchPage.width * 0.2, searchPage.width * 0.7)
      gradient: Gradient {
        GradientStop { position: 0.0; color: "#404040" }
        GradientStop { position: 0.7; color: "#121212" }
      }
    }

    Column {
        // target id
        id:contentColumn
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        height:100
        anchors.bottomMargin: 20

        SearchBar {
                id: titleMovie
                onAccepted: {
                    console.log(titleMovie.text)
                    //logic.searchMovies(titleMovie.text)
                    delay(500, function() {
                        logic.searchMoviesFetchDetail(titleMovie.text)

                         })

                }
        }
    }


    Column {
        id:grid
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        height: parent.height
        width: parent.width
        anchors.topMargin: 80
        //Grid View
        GridView {
          id: movieslist
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          height: parent.height
          anchors.topMargin: dp(10)
          cellWidth: widths()
          cellHeight: cellWidth


          model: JsonListModel{
              id : listModelSearch
              source: dataModel.searchMovies
              fields: ["id", "title", "poster"]
          }

          delegate: Item {
                     width: movieslist.cellWidth +10
                     height: movieslist.cellHeight + 30
                     scale: 0.8
                     id:delegateitem
                     Rectangle {
                         anchors.fill: parent
                         radius: dp(12)
                         border.color: "black"
                         border.width: dp(3)

             Image { source: poster
              anchors.fill: parent
              scale: 1
              y: 20;
              anchors.horizontalCenter: parent.horizontalCenter

          //hover image
              id: im1

                  states: [ "mouseIn", "mouseOut" ]
                  state: "mouseOut"

                  transitions: [
                      Transition {
                          from: "*"
                          to: "mouseIn"
                          NumberAnimation {
                              target: im1
                              properties: "scale"
                              from: 0.95
                              to: 1
                              duration: 400
                              easing.type: Easing.OutBounce
                          }
                      }
                  ]

                  MouseArea{
                      id: im1MouseArea
                      hoverEnabled: true
                      anchors.fill: parent

                      onContainsMouseChanged: {
                          im1.state = containsMouse ? "mouseIn" : "mouseOut"
                      }

                      onClicked: {
                          console.debug("click search")
                          delay(500, function() {
                              page.navigationStack.popAllExceptFirstAndPush(addPageComponent, {movie_id: model.id })

                               })
                      }
                  }
               }
             }
          }
        }
    }


    Timer {
        id: timer
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    // function to control the screen resolution
    function  widths(){

          if (Window.width <400)
              {
            return page.width / 2
          }
          else if  (Window.width >= 400 && Window.width < 600)
          {
             return page.width / 3
          }
          else if  (Window.width >= 600 && Window.width < 800)
          {
             return page.width / 4
          }
          else if  (Window.width >= 800 && Window.width < 1200)
          {
             return page.width / 5
          }
          else if  (Window.width > 1200)
          {
             return page.width / 10
          }
      }


    // component for creating detail pages
      Component {
          id: addPageComponent
          AddPage { }
    }
}
