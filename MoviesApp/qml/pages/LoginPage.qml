import Felgo 3.0
import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

NavigationStack{
    Page {
        id: loginPage
        navigationBarTranslucency: 1
        useSafeArea: false

        Rectangle {
          z: 1
          width: parent.width
          height: Theme.statusBarHeight
          color: "black"
          opacity: 1
        }


        LinearGradient {
          anchors.fill: parent

          start: Qt.point(0, 0)
          end: Qt.point(loginPage.width * 0.2, loginPage.width * 0.7)
          gradient: Gradient {
            GradientStop { position: 0.0; color: "#404040" }
            GradientStop { position: 0.7; color: "#121212" }
          }
        }

        Column {
                id: popularColumn

                width: parent.width
                spacing: dp(20)

                AppText {
                  font.bold: true
                  fontSize: 24
                  leftPadding: dp(Theme.contentPadding)
                  text: "Movies application"
                  color: 'white'

                }
        }


        // login form background
        Rectangle {
            id: loginForm
            anchors.centerIn: parent
            color: "white"
            opacity: 1
            width: content.width + dp(48)
            height: content.height + dp(16)
            radius: dp(4)


        }

        // login form content
        GridLayout {
            id: content
            anchors.centerIn: loginForm
            columnSpacing: dp(20)
            rowSpacing: dp(10)
            columns: 2

            // headline
            AppText {
                font.bold: true
                fontSize: 20
                Layout.topMargin: dp(8)
                Layout.bottomMargin: dp(12)
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignHCenter
                text: "Login"
            }



            // email text and field
            AppText {
                text: qsTr("E-mail")
                //font.pixelSize: sp(40)
                fontSize: 12

            }

            AppTextField {
                id: txtEmail
                Layout.preferredWidth: dp(150)
                showClearButton: true
                font.pixelSize: sp(11)
                borderColor: 'black'
                borderWidth: !Theme.isAndroid ? dp(2) : 0
            }

            // password text and field
            AppText {
                text: qsTr("Password")
                //font.pixelSize: sp(7)
                fontSize: 12

            }

            AppTextField {
                id: txtPassword
                Layout.preferredWidth: dp(150)
                showClearButton: true
                font.pixelSize: sp(11)
                borderWidth: !Theme.isAndroid ? dp(2) : 0
                echoMode: TextInput.Password
                borderColor: 'black'
            }

            Column {
                Layout.fillWidth: true
                Layout.columnSpan: 2
                Layout.topMargin: dp(3)
                width: loginForm.width
                // buttons
                AppButton {
                    backgroundColor: 'red'
                    text: qsTr("Login")
                    flat: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        loginPage.forceActiveFocus() // move focus away from text fields

                        // call login action
                        logic.loginFetchMovies(txtEmail.text, txtPassword.text)                    }
                }

                AppButton {
                    text: qsTr("No account yet? Register now")
                    textSize: 12
                    flat: true
                    //backgroundColor: 'red'
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        loginPage.forceActiveFocus() // move focus away from text fields

                        // call your logic action to register here
                        console.debug("registering...")
                        loginPage.navigationStack.push(registerPageComponent)

    //                   register()
                    }
                }
            }
        }




    }


    Component {
        id: registerPageComponent
        RegisterPage{}
      }

}
