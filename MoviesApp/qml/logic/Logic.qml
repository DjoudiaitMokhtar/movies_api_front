import QtQuick 2.0
import Felgo 3.0

Item {

    // actions
    signal fetchMovies()

    signal fetchMovieDetails(string movie_id)

    signal fetchMovieDetailsSearch(string movie_id)

    signal searchMoviesFetchDetail(string titleMovie)

    signal addMovie(string movie_id)

    signal deleteMovie(string movie_id)

    signal loginFetchMovies(string email, string password)

    signal searchMovies(string titleMovie)

    signal logout()

    signal register(string username, string email, string password)

    signal clearCache()

}
