import QtQuick 2.0
import Felgo 3.0

Item {
    // loading state
    readonly property bool busy: HttpNetworkActivityIndicator.enabled
    // configure request timeout
    property int maxRequestTimeout: 5000
    // initialization
    Component.onCompleted: {
        // immediately activate loading indicator when a request is started
        HttpNetworkActivityIndicator.setActivationDelay(0)
    }
    // private
        QtObject {
            id: _
            property string apiUrl: "http://127.0.0.1:8000/api/"

            function loginFetchMovies(data, success, error) {
                var p1 = Promise.create(function(resolve, reject) {
                    HttpRequest.post(apiUrl+'login/')
                     .timeout(maxRequestTimeout)
                     .set('Content-Type', 'application/json')
                     .send(data)
                     .end(function (err, res) {
                         if(res.ok)
                             resolve(res.body.token)
                         else
                             reject(err.message)
                     })
                });

                p1.then(function(value) {
                    var p2 = Promise.create(function(resolve, reject) {
                        HttpRequest.get(apiUrl)
                        .timeout(maxRequestTimeout)
                        .set( 'Authorization', 'Token ' + value )
                        .end(function (err, res) {
                            if(res.ok)
                                resolve({'res':res, 'token': 'Token ' + value})

                            else
                                reject(err.message)
                        });
                    });

               p2.then(function(value) {
                   // success
                   console.log("Value: "+value)
                   success(value)
                   var list_id = value.res.body
                   console.debug(list_id)
//                 for (let i = 0; i < list_id.length; i++) {
                   list_id.forEach((element, index, array) => {
                                       console.debug(element.movie_id)
                                       HttpRequest.get(apiUrl + 'detail/?id=' + element.movie_id)
                                       //.timeout(maxRequestTimeout)
                                       .set( 'Authorization', value.token )
                                       .end(function (err, res) {
                                           if(res.ok)
                                               //console.debug("movie_" + element.movie_id)
                                               //console.debug(res.status)
                                               cache.setValue("movie_" + element.movie_id, res.body)
                                       });
                                   })
               })
                    // failure
                    .catch(function(reason) {
                      console.log("Error: " + reason)
                    });
                });
            }

            function serachFetchDetail(token, title ,success, error){
                var p1 = Promise.create(function(resolve, reject) {
                    console.debug(token)
                    HttpRequest.get(apiUrl + 'search/?title_movie=' + title)
                    .timeout(maxRequestTimeout)
                    .set( 'Authorization', token )
                    .end(function (err, res) {
                        if(res.ok)
                            resolve({'res':res, 'token': token})

                        else
                            reject(err.message)
                    });
                });

                p1.then(function(value) {
                    // success
                    console.log("Value: "+value)
                    success(value)
                    var list_id = value.res.body
                    console.debug(list_id)
 //                 for (let i = 0; i < list_id.length; i++) {
                    list_id.forEach((element, index, array) => {
                                        console.debug(element.id)
                                        HttpRequest.get(apiUrl + 'detail/?id=' + element.id)
                                        //.timeout(maxRequestTimeout)
                                        .set( 'Authorization', value.token )
                                        .end(function (err, res) {
                                            if(res)
                                                console.debug("moviessearch_" + element.id)
                                                console.debug(res.status)
                                                cache.setValue("moviesearch_" + element.id, res.body)
                                        });
                                    })
                })

            }

            function post(url,data, token, success, error) {
             HttpRequest.post(url)
                .timeout(maxRequestTimeout)
                .set('Content-Type', 'application/json')
                .set( 'Authorization', token )
                .send(data)
                .then(function(res) { success(res)})
                .catch(function(err) { error(err) });
            }

            function fetch(url,token, success, error) {
                    HttpRequest.get(url)
                       .timeout(maxRequestTimeout)
                       .set( 'Authorization', token )
                       .then(function(res) { success(res) })
                       .catch(function(err) { error(err) });
                   }

            function del(url,token, success, error) {
                       HttpRequest.del(url)
                       .timeout(maxRequestTimeout)
                       .set( 'Authorization', token )
                       .set('Content-Type', 'application/json')

                       .then(function(res) { success(res.body) })
                       .catch(function(err) { error(err) });
                   }

        }

//     private rest api functions

        // register
            function register(user, success, error) {
                    _.post(_.apiUrl+'register/', user, success, error)
            }

        // login , get movies and detail movies
            function loginFetchMovies(user,success, error) {
                    _.loginFetchMovies(user, success, error)
            }


        // logout
            function logout(token, success, error) {
                _.fetch(_.apiUrl+'logout/', token, success, error)
            }


        // get detail movies
           function getDetailMovies(token, movie_id, success, error) {
               _.fetch(_.apiUrl+'detail/?id='+movie_id,token, success, error)
           }


        // get detail movies for serch movies
          function serachFetchDetail(token, title ,success, error) {
              _.serachFetchDetail(token, title ,success, error)
          }


        // get movies
            function fetchMovies(token,success, error) {
                    _.fetch(_.apiUrl,token, success, error)
            }


       // delete movie
            function delMovie(token, movie_id, success, error) {
                _.del(_.apiUrl+'detail/?id='+movie_id, token, success, error)
            }


       // search movies
            function search(token, title, success, error) {
                _.fetch(_.apiUrl+'search/?title_movie='+title, token, success, error)
            }


       // add movie
            function addMovie(token, movie_id, success, error) {
                _.post(_.apiUrl+'detail/?id=' + movie_id,token, success, error)
            }

}
